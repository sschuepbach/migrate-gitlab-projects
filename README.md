# Migrate Gitlab projects to another instance

This script helps migrating Gitlab projects recursively from one instance to
another Gitlab instance.

**==> Highly experimental. Use it at your own risk! <==**

## Prerequisites

- You have generated valid private access tokens allowing access to the old
  and new instance. Make sure that you (= your account) have sufficient rights
  to access private repositories in the group if you want them migrated
- You have exported your group (see
  [here](https://docs.gitlab.com/ee/user/group/settings/import_export.html#export-a-group)
  for details), the file is available locally.

## Configuration

The settings go into `config.json`:

- `source.instance`: Hostname of the origin GitLab instance
- `source.group`: The path to the origin root group (e.g. `swissbib/lab`)
- `source.group_export`: The path to the locally saved group export file
- `source.api_token`: The API token for the source
- `target.instance`: Hostname of the target GitLab instance
- `target.group`: The path to the target root group
- `target.parent_group`: If the root group should be moved into an already
  existing group, add the path to this group. Otherwise leave empty or delete
  the field entirely
- `target.api_token`: The API token for the target

## Run

```sh
python migrate.py
```
