import os
import requests
import json
import time
import urllib.parse


class Project:
    def __init__(self, path):
        self.path = path
        self.export_initiated = False
        self.local_file_path = None
        self.error = None


def get_group_id(instance, group_name, token):
    group_url = f"{instance}/api/v4/groups/{group_name}"
    response = requests.get(group_url, headers={"PRIVATE-TOKEN": token})
    try:
        response = response.json()
        return response["id"]
    except:
        return None


def initiate_group_export(source_instance, source_group, source_token):
    export_groups_url = f"{source_instance}/api/v4/groups/{source_group}/export"
    response = requests.post(export_groups_url, headers={"PRIVATE-TOKEN": source_token})
    response = response.json()
    return response.get("message") and response["message"] == "202 Accepted"


def group_export(
    source_instance,
    source_group,
    source_token,
):
    export_groups_url = (
        f"{source_instance}/api/v4/groups/{source_group}/export/download"
    )
    response = requests.get(export_groups_url, headers={"PRIVATE-TOKEN": source_token})
    retries = 0
    while True:
        if retries >= 10:
            exit("Project export not available. Exiting")
        try:
            response_json = response.json()
            if response_json.get("error") and response_json["error"].startswith("404"):
                exit(
                    "Something went wrong while downloading the group export. Aborting"
                )
            elif response_json["message"].startswith(
                "The group export file is not available yet"
            ):
                msg = response_json["message"]
                print(f"{msg}. Waiting 30 seconds before retrying")
                time.sleep(30)
                retries += 1
            elif response_json["message"].startswith("404 Not found or has expired"):
                msg = response_json["message"]
                print(f"{msg}. Waiting 10 seconds before retrying")
                time.sleep(10)
                retries += 1
            elif response_json["message"].get("error") and response_json["message"][
                "error"
            ].startswith("This endpoint has been requested too many times."):
                msg = response_json["message"]["error"]
                print(f"{msg}. Waiting 10 seconds before retrying")
                time.sleep(10)
                retries += 1
            else:
                exit("Something strange happened... Aborting")
        except:
            break
    filename = "group_export.tar.gz"
    with open(filename, "wb") as f:
        f.write(response.content)


def import_group(filename, target_instance, target_group, target_parent_group, target_token):
    with open(filename, "rb") as f:
        import_groups_url = f"{target_instance}/api/v4/groups/import"
        group_name = target_group.split("/")[-1]
        data = {"path": group_name, "name": group_name}
        if target_parent_group:
            id = get_group_id(target_instance, target_parent_group, target_token)
            if id:
                data["parent_id"] = id
        files = {"file": (filename, f)}
        response = requests.post(
            import_groups_url,
            headers={"PRIVATE-TOKEN": target_token},
            data=data,
            files=files,
        )
        print(response.json())
    # os.remove(filename)

def get_all_projects_recursively(source_instance, source_group, source_token):
    projects = []
    get_projects_url = f"{source_instance}/api/v4/groups/{source_group}/projects"
    response = requests.get(
        get_projects_url,
        params={
            "simple": "true",
            "include_subgroups": "true",
            "per_page": "50",
            "pagination": "keyset",
            "order_by": "id",
            "sort": "asc",
        },
        headers={"PRIVATE-TOKEN": source_token},
    )
    response_json = response.json()
    for project in response_json:
        projects.append(Project(project["path_with_namespace"]))
    try:
        next_page = response.links["next"]["url"]
        if next_page:
            projects.extend(
                [
                    Project(proj["path_with_namespace"])
                    for proj in retrieve_next_page(next_page, source_token)
                ]
            )
    finally:
        return projects


def retrieve_next_page(url, token):
    items = []
    response = requests.get(url, headers={"PRIVATE-TOKEN": token})
    if response.links.get("next"):
        next_page = response.links["next"]["url"]
        items.extend(retrieve_next_page(next_page, token))
    items.extend(response.json())
    return items


def initiate_export(source_instance, project, source_token):
    project_path = urllib.parse.quote_plus(project.path)
    export_project_url = f"{source_instance}/api/v4/projects/{project_path}/export"
    response = requests.post(
        export_project_url, headers={"PRIVATE-TOKEN": source_token}
    )
    response = response.json()
    while True:
        if (
            response.get("message")
            and type(response["message"]) == dict
            and response["message"].get("error")
            and response["message"]["error"].startswith(
                "This endpoint has been requested too many times."
            )
        ):
            print("Too many requests on endpoint. Waiting 10 seconds")
            time.sleep(10)
            response = requests.post(
                export_project_url, headers={"PRIVATE-TOKEN": source_token}
            )
            response = response.json()
        else:
            break

    if response.get("message") and response["message"].startswith("20"):
        print(f"Export for {project.path} initiated")
        project.export_initiated = True
    else:
        print(f"Initiating export for {project.path} failed.")
        project.error = "Initiating export failed"


def check_export_availability(source_instance, project_path, source_token):
    project_path = urllib.parse.quote_plus(project_path)
    export_project_url = f"{source_instance}/api/v4/projects/{project_path}/export"
    response = requests.get(export_project_url, headers={"PRIVATE-TOKEN": source_token})
    response = response.json()
    if response["export_status"] == "finished":
        return response["_links"]["api_url"]
    else:
        return None


def export_project(
    project, download_url, target_instance, target_group, source_token, target_token
):
    response = None
    while True:
        response = requests.get(download_url, headers={"PRIVATE-TOKEN": source_token})
        try:
            response_json = response.json()
            if response_json["message"]["error"].startswith(
                "This endpoint has been requested too many times."
            ):
                print("Too many requests on endpoint. Waiting 10 seconds")
                time.sleep(10)
            else:
                break
        except:
            break
    filename = project.path.replace("/", "_") + ".gz"
    with open(filename, "wb") as f:
        f.write(response.content)
    with open(filename, "rb") as f:
        upload_url = f"{target_instance}/api/v4/projects/import"
        files = {
            "file": (filename, f),
        }
        project_name = project.path.rsplit("/", 1)[1]
        data = {"path": project_name, "namespace": target_group}
        response = requests.post(
            upload_url, headers={"PRIVATE-TOKEN": target_token}, files=files, data=data
        )
        os.remove(filename)
        response = response.json()
        if response.get("status") and response["status"] == "scheduled":
            return None
        if response.get("message"):
            project.error = response["message"]
        elif response.get("status") and response["status"] != "scheduled":
            status = response["status"]
            project.error = f"Strange import status: {status}"


if __name__ == "__main__":
    config = dict()
    with open("config.json") as f:
        config = json.load(f)
    source_instance = config["source"]["instance"]
    source_group = config["source"]["group"]
    source_group = urllib.parse.quote_plus(source_group)
    source_group_export = config["source"]["group_export"]
    source_token = config["source"]["api_token"]
    target_instance = config["target"]["instance"]
    target_group = config["target"]["group"]
    target_parent_group = config["target"].get("parent_group")
    target_token = config["target"]["api_token"]

    print("Exporting groups")
    # if not initiate_group_export(source_instance, source_group, source_token):
        # exit("Group export failed. Aborting")
    # group_export(
        # source_instance,
        # source_group,
        # source_token,
        # target_instance,
        # target_group,
        # target_parent_group,
        # target_token,
    # )
    import_group(source_group_export, target_instance, target_group, target_parent_group, target_token)
    
    print("Groups exported")
    print("Fetching projects")
    projects = get_all_projects_recursively(source_instance, source_group, source_token)
    print(f"{len(projects)} projects fetched")
    print("Initiating exports")
    for project in projects:
        initiate_export(source_instance, project, source_token)
    print("Exporting projects")
    while True:
        for project in filter(
            lambda p: not p.local_file_path,
            filter(lambda p: p.export_initiated, projects),
        ):
            project.local_file_path = check_export_availability(
                source_instance, project.path, source_token
            )
            if project.local_file_path:
                ret_value = export_project(
                    project,
                    project.local_file_path,
                    target_instance,
                    target_group,
                    source_token,
                    target_token,
                )
                if ret_value:
                    print(f"Export of {project.path} failed: {ret_value}")
                else:
                    print(f"{project.path} successfully exported")
        if (
            len(
                list(
                    filter(
                        lambda p: not p.local_file_path,
                        filter(lambda p: p.export_initiated, projects),
                    )
                )
            )
            == 0
        ):
            break
        time.sleep(5)
    print("All finished")
    for project in filter(lambda p: p.error, projects):
        f"Error in project {project.path}: {project.error}"
